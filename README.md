# solution

Solution example for the Final challenge

1. Env file
```
mv .env.example .env
```
Change `PRIVKEY` to priv key

2. Build

```console
$ docker build -t n2n-oracle . 
```

3. Start

+ deploy
```console
$ docker run -ti --rm --env-file .env n2n-oracle /deployment/run.sh
```

+ oracle

```console
$ docker build -t n2n-oracle . ; docker-compose up --build oracle redis
```
