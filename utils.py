
def exit_with_message(message, exit_code=44, done=True):
    message = message.strip()
    if not done:
        message += ' Nothing to do.'
    print(message)
    exit(exit_code)