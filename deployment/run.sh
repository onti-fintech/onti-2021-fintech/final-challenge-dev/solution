#!/bin/sh
# change working directory
cd ${WORKDIR}/deployment

# flat the contracts
./flatten.sh > /dev/null

if [ "$VERIFY" = "true" ];
then
    python deploy.py --verify #2> /dev/null
else
    python deploy.py
fi
