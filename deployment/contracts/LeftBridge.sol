// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./BaseBridge.sol";
import "./AddressRecoveryTool.sol";

contract LeftBridge is BaseBridge, AddressRecoveryTool {

    constructor (address validatorContract, address _owner) {
        _setValidatorContract(validatorContract);
        _setOwner(_owner);
        setMinPerTx(0 ether);
        setMaxPerTx(0 ether);
        startOperations();
    }

    function getLiquidityLimit() public view returns (uint256){
        return _balanceOnDiffBridge();
    }

    function updateLiquidityLimit(uint256 newlimit) public onlyOwner {
        require(newlimit >= 0);
        m_balanceOnDiffBridge = newlimit;
    }

    function commit(address payable _recipient, uint256 _amount, bytes32 _txHash) external onlyValidator {
        require(!m_robustMode, "robust mode");
        _commit(_recipient, _amount, _txHash);
    }

    function applyCommits(address recipient, uint256 amount, bytes32 id, uint256[] memory r, uint256[] memory s, uint8[] memory v) external {
        require(isRobustMode(), "Not robust mode");
        require((r.length == s.length) && (s.length == v.length));
        require(requiredSignatures() == r.length);

        

        uint256 signed;

        for (uint256 i = 0; i < r.length; i++) {
            address address_ = recover(recipient, amount, id, uint8(v[i]), bytes32(r[i]), bytes32(s[i]));
            
            require(isValidator(address_), string(abi.encodePacked(address_, " not a validator")));

            require(!_isAlreadyProcessed(signed), "This transaction was already submitted");

            require(!_accountHasSigned(id, address_), "This account already submitted sign");
            
            signed = _numMessagesSigned(id);
            
            _setAccountSigned(id, address_, true);

        }
        
        m_numMessagesSigned[id] = _markAsProcessed(signed);
        
        sendNativeTokens(payable(recipient), amount);
        emit MessageConfirmed(recipient, amount, id);
    }
}
