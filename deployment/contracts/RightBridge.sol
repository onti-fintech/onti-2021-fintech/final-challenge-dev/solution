// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./BaseBridge.sol";
import "./AddressRecoveryTool.sol";


contract RightBridge is BaseBridge, AddressRecoveryTool {
    struct RobustAction {
        address recipient;
        uint256 amount;
        uint256[] r;
        uint256[] s;
        uint8[] v;
        address assignee;
    }

    event CommitRegistered(address indexed signer, bytes32 messageHash);
    event commitsCollected(bytes32 id, uint8 commits);


    mapping(bytes32 => RobustAction) m_actions;

    function getTransferDetails(bytes32 txHash) public view returns (address recipient, uint256 amount) {
        recipient = m_actions[txHash].recipient;
        amount = m_actions[txHash].amount;
    }

    function getCommit(bytes32 txHash, uint8 index) public view returns (uint256 r, uint256 s, uint8 v) {
        r = m_actions[txHash].r[index];
        s = m_actions[txHash].s[index];
        v = m_actions[txHash].v[index];
    }

    constructor (address validatorContract, address _owner) {
        _setValidatorContract(validatorContract);
        _setOwner(_owner);
        setMinPerTx(0 ether);
        setMaxPerTx(0 ether);
        startOperations();
    }

    function addLiquidity() payable external onlyOwner {
        require(msg.value > 0, "Zero value");
    }

    function commit(address payable _recipient, uint256 _amount, bytes32 _txHash) external onlyValidator {
        _commit(_recipient, _amount, _txHash);
    }


    function registerCommit(address recipient, uint256 amount, bytes32 txHash, uint256 r, uint256 s, uint8 v) external onlyValidator checkLimit(amount) {
        require(m_robustMode, "Not robust mode");
        require(recover(recipient, amount, txHash, v, bytes32(r), bytes32(s)) == msg.sender);
        uint256 signed = _numMessagesSigned(txHash);

        require(!_isAlreadyProcessed(signed), "This transaction was already submitted");

        require(!_accountHasSigned(txHash, msg.sender), "This account already submitted sign");

        _setAccountSigned(txHash, msg.sender, true);
        uint256 reqSigs = requiredSignatures();

        if (signed == 0) {
            m_actions[txHash].r = new uint256[](reqSigs);
            m_actions[txHash].s = new uint256[](reqSigs);
            m_actions[txHash].v = new uint8[](reqSigs);
        }

        m_actions[txHash].r[signed] = r;
        m_actions[txHash].s[signed] = s;
        m_actions[txHash].v[signed] = v;
        m_actions[txHash].amount = amount;
        m_actions[txHash].recipient = recipient;

        signed += 1;
        emit CommitRegistered(msg.sender, txHash);

        if (signed >= reqSigs) {
            m_numMessagesSigned[txHash] = _markAsProcessed(signed);
            m_actions[txHash].assignee = msg.sender;
            emit commitsCollected(txHash, uint8(signed));
        }
        else {
            m_numMessagesSigned[txHash] = signed;
        }
    }

    function getAssignee(bytes32 txHash) public view returns(address assignee){
        assignee = m_actions[txHash].assignee;
    }

    function getLiquidityLimit() public view returns (uint256){
        return address(this).balance;
    }
}

