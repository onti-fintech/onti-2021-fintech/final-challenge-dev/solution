pragma solidity ^0.7.6;


import "./IBridgeValidators.sol";


contract Validable {
    address m_validatorContract;
    uint256 minLimit;
    uint256 maxLimit;
    bool isBridgeUp;

    function _setValidatorContract(address _validatorContract) internal {
        require(_validatorContract != m_validatorContract);
        m_validatorContract = _validatorContract;
    }
    
    function validatorContract() public view returns (IBridgeValidators) {
        return IBridgeValidators(m_validatorContract);
    }
    function isValidator(address validator) public view returns (bool){
        return validatorContract().isValidator(validator);
    }

    modifier onlyValidator() {
        require(isValidator(msg.sender));
        _;
    }

    modifier checkLimit(uint256 amount){

        if (maxLimit == 0){
            require(amount > minLimit);
        } else {
            require(amount > minLimit && amount < maxLimit);
        }
        _;
    }
    
    function requiredSignatures() public view returns (uint256) {
        return validatorContract().getThreshold();
    }

    modifier bridgeIsUp(){
        require(isBridgeUp);
        _;
    }
}
