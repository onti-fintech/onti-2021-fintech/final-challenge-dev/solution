// SPDX-License-Identifier: MIT

pragma solidity ^0.7.6;

import "./Ownable.sol";
import "./Validable.sol";
import "./Address.sol";


contract BaseBridge is Ownable, Validable {
    struct Action{
        uint256 numMessagesSigned;
    }

    event bridgeActionInitiated(address recipient, uint256 amount);
    event SignedForUserRequest(address indexed signer, bytes32 messageHash);
    event MessageConfirmed(address recipient, uint256 value, bytes32 transactionHash);

    mapping (bytes32 => uint256) m_numMessagesSigned;
    mapping (bytes32 => bool) m_signed;



    uint256 m_balanceOnDiffBridge;

    bool m_robustMode = false;

    receive() payable external {
        require(msg.data.length == 0);
        nativeTransfer(msg.sender);
    }

    function isRobustMode() public view returns (bool) {
        return m_robustMode;
    }

    function _balanceOnDiffBridge() internal view returns (uint256){
        return m_balanceOnDiffBridge;
    }

    function nativeTransfer(address _receiver) internal checkLimit(msg.value) bridgeIsUp{
        uint256 valueToTransfer = msg.value;
        require(_balanceOnDiffBridge() >= valueToTransfer);
        m_balanceOnDiffBridge -= valueToTransfer;

        emit bridgeActionInitiated(_receiver, valueToTransfer);
    }

    function _numMessagesSigned(bytes32 _transactionId) public view returns (uint256){
        return m_numMessagesSigned[_transactionId];
    }

    function _accountHasSigned(bytes32 _transactionId, address _validator) public view returns (bool){
        return m_signed[keccak256(abi.encodePacked(_transactionId, _validator))];
    }

    function _setAccountSigned(bytes32 _transactionId, address _validator, bool _value) internal{
        m_signed[keccak256(abi.encodePacked(_transactionId, _validator))] = _value;
    }

    function _isAlreadyProcessed(uint256 _number) internal pure returns (bool) {
        return _number & (2**255) == 2**255;
    }

    function _markAsProcessed(uint256 _v) internal pure returns (uint256) {
        return _v | (2**255);
    }

    function _commit(address payable _recipient, uint256 _amount, bytes32 _txHash) internal checkLimit(_amount){

        bytes32 transactionId = _txHash;

        uint256 signed = _numMessagesSigned(transactionId);

        require(!_isAlreadyProcessed(signed), "This transaction was already submitted");

        require(!_accountHasSigned(transactionId, msg.sender), "This account already submitted sign");

        _setAccountSigned(transactionId, msg.sender, true);
        signed += 1;
        emit SignedForUserRequest(msg.sender, transactionId);

        uint256 reqSigs = requiredSignatures();
        if (signed >= reqSigs){
            m_numMessagesSigned[transactionId] = _markAsProcessed(signed);
            m_balanceOnDiffBridge += _amount;
            emit MessageConfirmed(_recipient, _amount, _txHash);
            sendNativeTokens(_recipient, _amount);
            // TODO: remove all values in m_signed for every validator?
        } else {
            m_numMessagesSigned[transactionId] = signed;
        }
    }

    function sendNativeTokens(address payable _recipient, uint256 _amount) internal {
        Address.safeSendValue(_recipient, _amount);
    }

    function changeValidatorSet(address newvalidatorset) public onlyOwner {
        _setValidatorContract(newvalidatorset);
    }

    function enableRobustMode() public onlyOwner {
        m_robustMode = true;
    }

    function getMaxLimit() public view returns (uint256){
        return maxLimit;
    }

    function getMinLimit() public view returns (uint256){
        return minLimit;
    }

    function setMaxPerTx(uint256 limit) public onlyOwner{
        maxLimit = limit;
    }

    function setMinPerTx(uint256 limit) public onlyOwner{
        minLimit = limit;
    }

    function stopOperations() public onlyOwner{
        isBridgeUp = false;
    }

    function startOperations() public onlyOwner{
        isBridgeUp = true;
    }

}