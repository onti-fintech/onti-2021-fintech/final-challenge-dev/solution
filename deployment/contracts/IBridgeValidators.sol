pragma solidity ^0.7.6;

interface IBridgeValidators {
    function isValidator(address _validator) external view returns (bool);
    function getThreshold() external view returns (uint256);
    function owner() external view returns (address);
}