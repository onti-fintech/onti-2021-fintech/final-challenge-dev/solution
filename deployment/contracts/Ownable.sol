pragma solidity ^0.7.6;


contract Ownable{
    address m_owner;
    event OwnershipTransferred(address previousOwner, address newOwner);
    
    
    modifier onlyOwner() {
        require(msg.sender == m_owner, "Not owner");
        /* solcov ignore next */
        _;
    }

    /**
    * @dev Allows the current owner to transfer control of the contract to a newOwner.
    * @param newOwner the address to transfer ownership to.
    */
    function transferOwnership(address newOwner) external onlyOwner {
        require(newOwner != m_owner);
        _setOwner(newOwner);
    }

    /**
    * @dev Sets a new owner address
    */
    function _setOwner(address newOwner) internal {
        require(newOwner != address(0), "Zero address for setOwner");
        emit OwnershipTransferred(m_owner, newOwner);
        m_owner = newOwner;
    }
}
