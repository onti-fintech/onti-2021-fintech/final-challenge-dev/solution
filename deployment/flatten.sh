if [ -d flats ]; then
  rm -rf flats
fi

FLATTENER=solu
VERSION=0.7.6

${FLATTENER} contracts/LeftBridge.sol ${VERSION} -o LeftBridge_flat.sol
${FLATTENER} contracts/RightBridge.sol ${VERSION} -o RightBridge_flat.sol
${FLATTENER} contracts/BridgeValidators.sol ${VERSION} -o BridgeValidators_flat.sol

mv out flats
