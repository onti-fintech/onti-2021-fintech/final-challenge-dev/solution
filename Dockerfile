FROM python:3.9-alpine

COPY --from=ethereum/solc:0.7.6-alpine  /usr/local/bin/solc /usr/local/bin/solc

RUN apk update && apk upgrade
# Iptables.
RUN apk add iptables
# GCC.
RUN apk add --no-cache --virtual .build-deps gcc musl-dev

RUN python -m pip install --upgrade pip
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

# Remove gcc.
RUN apk del .build-deps
# Remove cache.
RUN python -m pip cache purge

# workdir in root directory
ENV WORKDIR "/"
ENV PYTHONPATH "${PYTHONPATH}:/"

COPY deployment/ deployment/
WORKDIR /deployment/
RUN /bin/sh ./flatten.sh

WORKDIR /
COPY . .

