import os

from dotenv import load_dotenv
from web3 import Web3

load_dotenv('.env')

PRIVKEY = os.environ.get('PRIVKEY')

LEFT_RPCURL = os.environ.get('LEFT_RPCURL')

LEFT_GASPRICE = int(os.environ.get('LEFT_GASPRICE', '-1'))

LEFT_ADDRESS = os.environ.get('LEFT_ADDRESS', '')
if LEFT_ADDRESS:
    LEFT_ADDRESS = Web3.toChecksumAddress(LEFT_ADDRESS)

LEFT_START_BLOCK = int(os.environ.get('LEFT_START_BLOCK', '-1'))

RIGHT_RPCURL = os.environ.get('RIGHT_RPCURL')

RIGHT_GASPRICE = int(os.environ.get('RIGHT_GASPRICE', '-1'))

RIGHT_ADDRESS = os.environ.get('RIGHT_ADDRESS', '')
if RIGHT_ADDRESS:
    RIGHT_ADDRESS = Web3.toChecksumAddress(RIGHT_ADDRESS)

RIGHT_START_BLOCK = int(os.environ.get('RIGHT_START_BLOCK', '-1'))

VALIDATORS = os.environ.get('VALIDATORS', '').split()

THRESHOLD = int(os.environ.get('THRESHOLD', '-1'))

LOCAL = os.environ.get('LOCAL', 'false')

DEFAULTGAS = 2000001

TIMEOUT_RECEIPT_CHECKER = 12000

REQUIRED_CONFIRMATIONS = int(os.environ.get('REQUIRED_CONFIRMATIONS', '0'))

CONTRACT_VALIDATORS = {
    # deployment directory
    'path': 'flats/BridgeValidators_flat.sol',
    
    'name': 'BridgeValidators',
    'abi_of_args': ['address[]', 'uint256', 'address'],
}

CONTRACT_LEFT = {
    # deployment directory
    'path': 'flats/LeftBridge_flat.sol',
    
    'name': 'LeftBridge',
    'abi_of_args': ['address', 'address'],
}

CONTRACT_RIGHT = {
    # deployment directory
    'path': 'flats/RightBridge_flat.sol',
    
    'name': 'RightBridge',
    'abi_of_args': ['address', 'address'],
}


REDIS_CONFIG = {
    'host': 'redis',
    'port': 5432,
}
