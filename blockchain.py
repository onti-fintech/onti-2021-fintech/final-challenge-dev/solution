import logging
import os
import re
import time
from subprocess import check_output
from typing import Tuple

import requests
from eth_abi import encode_abi
from eth_account.signers.local import LocalAccount
from solcx import compile_files
from web3 import Web3, HTTPProvider, Account
from web3.exceptions import TransactionNotFound
from web3.middleware import geth_poa_middleware

from config import (
    LEFT_GASPRICE, RIGHT_GASPRICE,
    LEFT_RPCURL, RIGHT_RPCURL,
    PRIVKEY, DEFAULTGAS
)

if PRIVKEY:
    account = Account.privateKeyToAccount(PRIVKEY)
else:
    account = None


def get_prepared_web3(rpc_url: str) -> Web3:
    web3 = Web3(HTTPProvider(rpc_url))
    web3.middleware_onion.inject(geth_poa_middleware, layer=0)
    if account:
        web3.eth.defaultAccount = account.address
    return web3


web3_left = get_prepared_web3(LEFT_RPCURL)
web3_right = get_prepared_web3(RIGHT_RPCURL)

LEFT_CONNECTED = web3_left.isConnected()
RIGHT_CONNECTED = web3_right.isConnected()
web3_left.eth.custom_defaultGasPrice = LEFT_GASPRICE
web3_right.eth.custom_defaultGasPrice = RIGHT_GASPRICE

if PRIVKEY:
    account = web3_right.eth.account.from_key(PRIVKEY)
    web3_left.eth.defaultAccount = account.address
    web3_right.eth.defaultAccount = account.address
else:
    account = None

ZERO_ADDRESS = '0x' + '0' * 40
normal_addr = Web3.toChecksumAddress
keccak = Web3.keccak


class TransactionReverted(Exception):
    pass


class NotEnoughFunds(Exception):
    def __init__(self, address, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.address = address


def default_nonce(web3, account):
    return web3.eth.getTransactionCount(account.address)


def get_deployed_contract(web3, address, abi):
    return web3.eth.contract(address=address, abi=abi)


def balanceOf(web3, address):
    return web3.eth.getBalance(address)


def extract_solc_version() -> str:
    output = check_output(["solc", "--version"]).decode()
    result = re.search(r"Version: (.+)\.Linux\.g\+\+", output)
    assert result, output
    version: str = result.group(1)
    if not version.startswith('v'):
        version = 'v' + version

    return version


def compile_contract(contract_path, contract_name) -> Tuple[str, dict]:
    """
    Return bytecode and abi of 
    `contract_name` in `contract_path` file
    """
    if not os.path.exists(contract_path):
        raise FileNotFoundError(contract_path)
    artifacts = compile_files(contract_path, optimize=True, solc_binary='solc', output_values=['abi', 'bin'])
    compiled = artifacts.get(f'{contract_path}:{contract_name}')

    return compiled['bin'], compiled['abi']


def deploy_contract(web3, path, name, account=account, gas=None, args=(), abi_of_args=None, verify=False):
    """
    Deploy contract with given bytecode and abi
    using `account`

    `args` will be passed to contract constructor

    If `verify` is true, then try to verify contract on blockscout

    Return transaction receipt of contract deployment
    """
    contract_bytecode, contract_abi = compile_contract(path, name)
    contract = web3.eth.contract(bytecode=contract_bytecode, abi=contract_abi)
    try:
        tx = contract.constructor(*args).buildTransaction()
    except ValueError as e:
        logging.error(f"Cannot evaluate gas for constructor with args {args}")
        logging.error(e)
        return

    data = tx['data']
    gas = gas or tx['gas']

    txReceipt = build_and_send_tx(web3, _from=account, to='', data=data, gas=gas)

    assert txReceipt['status']
    contractAddress = txReceipt['contractAddress']

    logging.info(f'Contract {name} deployed at {contractAddress}')

    if verify:
        with open(path) as file:
            source_code = file.read()

        abiEncodedArgs = encode_abi(abi_of_args, args).hex()
        contract_info = {
            "address": contractAddress,
            "source_code": source_code,
            "name": name,
            "compiler_version": extract_solc_version(),
            "constructor_arguments": abiEncodedArgs,
        }
        logging.debug(f"Contract info: {contract_info}")
        verified = verify_contract(
            **contract_info
        )

        if verified:
            logging.info(f'Contract {name} is verified')
        else:
            raise ValueError(f"Contract is not verified. address: {contractAddress}.")

    return txReceipt


def verify_contract(address, source_code, compiler_version, name, constructor_arguments, optimization=200) -> bool:
    """
    Try to verify contract
    """
    url = 'https://blockscout.com/poa/sokol/api?module=contract&action=verify'
    data = {
        "addressHash": address,
        "compilerVersion": compiler_version,
        "contractSourceCode": source_code,
        "name": name,
        "optimization": True if optimization else False,
        "optimizationRuns": optimization,
        "constructorArguments": constructor_arguments,
        "evmVersion": 'default',
    }
    length = len(source_code)
    logging.info(
        f"Start verifying contract {name}, address={address}, source code length={length}")

    MAX_REQUESTS = 10
    # in seconds
    DELAY_BETWEEN_REQUESTS = 10
    for _ in range(MAX_REQUESTS):
        response = requests.post(url, json=data, timeout=120)
        logging.info(f"Got response {response}")

        verified = response.ok

        if not verified:
            logging.info(f"Sleep for {DELAY_BETWEEN_REQUESTS} seconds")
            time.sleep(DELAY_BETWEEN_REQUESTS)
        else:
            break

    return verified


def build_and_send_tx(web3, _from, to, data='', value=0, gas=None, gasPrice=None, nonce=None):
    """
    Build, sign, send transaction and wait for its receipt
    """

    if not isinstance(_from, LocalAccount):
        _from = web3.eth.account.from_key(_from)

    nonce = nonce or default_nonce(web3, _from)
    tx = {
        'from': _from.address,
        'to': to,
        'nonce': nonce,
        'data': data or '',
        'value': value,
        'gasPrice': gasPrice or web3.eth.custom_defaultGasPrice,
        'gas': gas or DEFAULTGAS
    }

    return send_tx(web3, tx)


def send_tx(web3, tx, account=account):
    if 'nonce' not in tx:
        tx['nonce'] = default_nonce(web3, account)

    signed = account.signTransaction(tx)
    try:
        tx_hash = web3.eth.sendRawTransaction(signed.rawTransaction)
    except ValueError as e:
        if tx['gasPrice'] * tx['gas'] > web3.eth.getBalance(account.address):
            raise NotEnoughFunds(address=account.address)
        else:
            raise e
    return wait_tx_receipt(web3, tx_hash)


def wait_tx_receipt(web3, tx_hash, sleep_interval=0.5, max_tries=20):
    """
    Wait for transaction receipt with given sleep interval
    Stop after `max_tries` failed attempts
    """

    failed = 0
    while failed < max_tries:
        try:
            tx_receipt = web3.eth.getTransactionReceipt(tx_hash)
        except TransactionNotFound:
            tx_receipt = None
        if tx_receipt:
            return tx_receipt
        failed += 1
        time.sleep(sleep_interval)

    logging.error(f"Can not extract transaction")
