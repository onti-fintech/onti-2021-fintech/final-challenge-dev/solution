from blockchain import web3_left, web3_right
from config import LEFT_ADDRESS, RIGHT_ADDRESS, \
    RIGHT_START_BLOCK, LEFT_START_BLOCK
from contract_abi import RIGHT_ABI, LEFT_ABI




def init():
    left_contract = web3_left.eth.contract(address=LEFT_ADDRESS, abi=LEFT_ABI)
    right_contract = web3_right.eth.contract(address=RIGHT_ADDRESS, abi=RIGHT_ABI)
    left_STOPBLOCK = LEFT_START_BLOCK
    right_STOPBLOCK = RIGHT_START_BLOCK

    try:
        pass #read from database
    except Exception:
        pass
    left_filter = left_contract.events.bridgeActionInitiated.createFilter(fromBlock=left_STOPBLOCK, toBlock='latest')
    right_filter = right_contract.events.bridgeActionInitiated.createFilter(fromBlock=right_STOPBLOCK, toBlock='latest')
    return [left_filter, right_filter]


def output_compose(eventlist, side):
    data = []
    for txn in eventlist:
        recipient, txn_hash = None, None
        try:
            recipient, txn_hash = txn['args']['recipient'], txn['transactionHash']

        except Exception:
            pass
        data.append({'recipient': recipient, 'txn_hash': txn_hash, "side": side})
    return data


def reed_blockchain():
    left_filter, right_filter = init()
    final_data = []
    left_eventlist = left_filter.get_all_entries()
    right_eventlist = right_filter.get_all_entries()
    if len(left_eventlist):
        final_data += output_compose(left_eventlist, 'left')
    if len(right_eventlist):
        final_data += output_compose(right_eventlist, 'right')

    return final_data

