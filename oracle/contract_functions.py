from typing import Tuple
import logging
from blockchain import send_tx

def call_function(contract, function_name, *args):
    logging.info(f"Call {function_name}{args}")    
    return contract.functions[function_name](*args).call()

def isValidator(contract, address):
    """
    Returns true if address is validator in left of right side
    """
    return call_function(contract, 'isValidator', address)


def isRobustMode(contract) -> bool:
    return call_function(contract, 'isRobustMode')


def getTransferDetails(contract, txHash: bytes) -> Tuple[str, int]:
    """
    Return recipient and amount
    """
    return call_function(contract, 'getTransferDetails', txHash)

def getCommit(contract, txHash: bytes, index: int) -> Tuple[int, int, int]:
    """
    Return r, s, v
    """
    return call_function(contract, 'getCommit', txHash, index)


def custom_getCommitsAmount(contract, txHash):
    if txHash.startswith('0x'):
        txHash = txHash[2:]
    events = contract.events.commitsCollected().createFilter(
        fromBlock=0, 
        argument_filters={
            'id': bytes.fromhex(txHash)
        }).get_all_entries()
    assert events, "Not such txHash"

    event = events[0]

    return event['args']['commits']


def transact_contract_function(web3, contract, account, function_name, *args):
    function = contract.functions[function_name](*args)
    
    tx = function.buildTransaction()
    
    return send_tx(web3, tx, account=account)

def applyCommits(web3, contract, account, recipient, amount, id, r, s, v):
    return transact_contract_function(web3, contract, account, 'applyCommits', recipient, amount, id, r, s, v)
