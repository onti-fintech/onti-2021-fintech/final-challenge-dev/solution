from config import CONTRACT_LEFT, CONTRACT_RIGHT
from blockchain import compile_contract

CONTRACT_RIGHT['path'] = '../deployment/' + CONTRACT_RIGHT['path']
CONTRACT_LEFT['path'] = '../deployment/' + CONTRACT_LEFT['path']

_, RIGHT_ABI = compile_contract(CONTRACT_RIGHT['path'], CONTRACT_RIGHT['name'])
_, LEFT_ABI = compile_contract(CONTRACT_LEFT['path'], CONTRACT_LEFT['name'])
