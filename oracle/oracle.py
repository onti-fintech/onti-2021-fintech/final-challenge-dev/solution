from signer_worker import OracleManager
from config import RIGHT_RPCURL, RIGHT_ADDRESS , RIGHT_GASPRICE
from blockchain import web3_right, account, normal_addr, LEFT_CONNECTED, RIGHT_CONNECTED
from contract_abi import RIGHT_ABI
import time
import logging

import random
import os

if not LEFT_CONNECTED or not RIGHT_CONNECTED:
    print(f'ERROR: NOT CONNECTED TO RPC: {LEFT_CONNECTED, RIGHT_CONNECTED}')
    exit()

logging.basicConfig(level=logging.DEBUG)

with OracleManager(builders=2) as manager:
    manager.join()
    
