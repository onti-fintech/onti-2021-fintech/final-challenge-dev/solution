from multiprocessing import Value
from time import sleep, time

from web3.exceptions import TransactionNotFound

from blockchain import get_prepared_web3
from config import TIMEOUT_RECEIPT_CHECKER as TIMEOUT
from .constants import Queue, receipt_checker_logger as logger

def receipt_checker_worker(rpc_url: str, is_running: Value, input_queue: Queue, sender_queue: Queue) -> None:
    """
    Watches for event forever
    """
    web3 = get_prepared_web3(rpc_url)

    while is_running.value:

        if not input_queue.empty():
            job = input_queue.get()
            tx, tx_hash, tx_time = job
            try:
                tx_from_node = web3.eth.getTransaction(tx_hash)
                logger.info(f"check transaction for block: {tx_from_node['blockNumber']}")
                if not tx_from_node['blockNumber']:
                    if time() - tx_time >= TIMEOUT:
                        logger.info('Put transaction back to sender input')
                        sender_queue.put(tx)
                    else:
                        input_queue.put(job)
                else:
                    txReceipt = web3.eth.getTransactionReceipt(tx_hash)
                    logger.info(f"Transaction is done, status: {txReceipt['status']}")
            except TransactionNotFound:
                logger.info('Transaction not found. Put back to sender input')
                sender_queue.put({'tx' : tx})
        else:
            sleep(0.1)
