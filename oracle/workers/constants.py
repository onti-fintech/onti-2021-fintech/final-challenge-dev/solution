import logging

from queue import Queue

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -15s %(funcName) -20s: %(message)s')
logging.basicConfig(format=LOG_FORMAT)

level = logging.INFO

def getLogger(name):
    logger =  logging.getLogger(name)
    logger.setLevel(level)
    return logger

watcher_logger = logging.getLogger('watcher')
watcher_logger.setLevel(level)

builder_logger = logging.getLogger('builder')
builder_logger.setLevel(level)

sender_logger = logging.getLogger('sender')
sender_logger.setLevel(level)

receipt_checker_logger = logging.getLogger('receipt_checker')
receipt_checker_logger.setLevel(level)

